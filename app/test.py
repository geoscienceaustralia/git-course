import unittest
from script import hello

class Test(unittest.TestCase):
    def testJohn(self):
        self.assertEqual(hello("John"), "Hello, John!")

if __name__ == '__main__':
    unittest.main()
